# micro-service 

## Contents

* [Description](#description)
* [Getting Started](#getting-started)
* [API Endpoints](#api-endpoints)
* [Configuration management](#configuration-management)
* [Environment vars](#environment-vars)
* [Test](#test)
* [Logging](#logging)
* [Metrics](#metrics)
* [Actuator](#actuator)
* [Dependency management](#dependency-management)
* [Maintenance and Contributing](#maintenance-and-contributing)
* [Licence](#licence)

Note that project started as "notifier" and was expected to poll a rabbit queue for amca clains/cofc (in the guise of an msCitizenSubmission) and then had a controller added
The intention being to work out the most appropriate party in a claim/cofc to contact, which template to use and then to request that a letter be sent

In late November 2020 it was re-magined as a service that would garner the relevant contacts for a citizen (the DP in the claim) and pass that back.  The caller would then decide who to contact

As such there will be elements of the code (and this read me) that hark back to the earlier incarnation of the project. Caveat emptor


## Description

A backend microservice written in Spring and Java, designed to gather information about a person and return details of people to contact about a claim made for CA for caring for them.
Currently a date of birth to go with the nino is required.  This is used to match the details retrieved for the nino so we can be sure we are being asked about the right citizen

The controller offers two endpoints that return essentially the same data - details of the citizen and other likely correspondents
/contacts       - returns an AmcaContactList object.  This consists of a "citizen" correspondent and "actingBodies" an array/list of correspondent entires for guardians/appointees/ect
/correspondents - returns a "flat" list of correspondents (citizen, guardians, appointees etc.  NOTE : the ciitizen will always be the last in the list

### Configuration

For flexibility (and whilst we don't absolutely understand how to identify an acting body) the filter/search criteria to identiify a qualifying personal Relationship or organisational Relationship are specified via confi
There is an "actingBodies" section in application.yaml that specifies for a few fields what values are acceptable to specify an acting body.  Wildcard is an option
(Ideally this would be flexible enough to allow fields (as well as their values) to be specified flexibly in config.  Maybe later)

## Getting Started

### Pre-requisites

In order to modify, commit, build and run the service you will need the following on your machine:
* Java 11
* Git
* Maven 3.6.0
* Docker

### Installing

Get started by first cloning the project from git. Once on your local machine the service can be run either within an IDE, using Docker or directly with java on the command line.

NOTE: you will need to repeat these steps whenever you pull an updated version of the code


##### Command line

To run on the command line, from the root of the project directory:

```bash  
mvn clean package  
java -jar target/ms-contactmanager*.jar
```

##### Docker

To run in Docker, from the root of the project directory:

```bash
mvn clean package
docker build -t ms-contactmanager -f docker/Dockerfile .
docker run --name notifier -p 9900:9900 -d --rm notifier  
```

To stop the service run:

```bash
docker stop ms-contactmanager
```

##### Docker Compose

To build and run the full service along with any external components (such as databases), do the following from project root:

```bash
mvn clean package 
docker-compose up --build
``` 

To stop the service with docker-compose do the following:

```bash
docker-compose down
```  

### Using the Service

Once started, you should be able to access the service on the following URL: `http://localhost:9900/notifier/`

To check that the service is up and running you can call the health endpoint on: : `http://localhost:9900/notifier/actuator/health`
If the service is healthy you will get a 200 response with a body giving a status of "UP".


## API Endpoints

#####  GET actuator/health

This endpoint allows you to check the health of the service. If the service is healthy the response will have a status 200 with a body similar to this:

```json
{
    "status": "UP",
    "components": {
        "diskSpace": {
            "status": "UP",
            "details": {
                "total": 250685575168,
                "free": 53228195840,
                "threshold": 10485760
            }
        },
        "ping": {
            "status": "UP"
        },
        "refreshScope": {
            "status": "UP"
        }
    }
}
```

If the service is unhealthy a status 503 will be returned.


#####  GET actuator/info

This endpoint provides service information. Calls to this endpoint will return a status 200 with a body similar to this:

```json

{
    "java": {
        "version": "11.0.1"
    },
    "build": {
        "artifact": "ms-contactmanager",
        "name": "notifier",
        "time": "2020-05-14T15:48:21.724Z",
        "version": "1.0.0-SNAPSHOT",
        "group": "uk.gov.dwp"
    }
}

```              

The information shown is generated by the spring-boot-maven-plugin during the compile stage. If you do not see any build information then you will need to run:

```bash
mvn compile
```


##### Error Responses:

If a client or server side error occurs when calling any of the above endpoints, an error response will be returned in the following format:

```json  
{  
    "httpStatus": "Bad Request",  
    "httpStatusCode": 400,  
    "timestamp": "2019-08-08T14:11:16.349762",  
    "errors": [  
        {  
            "message": "Validation error : Invalid nino format",  
            "path": "GET /myservice/api/v1/example", 
            "detailedMessage": "getExample.example: Invalid nino format"  
        }  
    ]   
}    
```    

*   All error responses included a detailedMessage field which will output the verbose error message produced by the system. If you want to disable this field from the response, all you need to do is set the environment variable DETAILED_MESSAGE_ENABLED to false when starting the service.


## Configuration management

Service configuration is managed externally through environment variables. These are defined within the application.yml file, with the values being set at run time.

The following section of the application.yml shows an example of this:

```yaml
server:
  port: ${PORT:9900}
  servlet:
    context-path: ${CONTEXT_PATH:/ms-contactmanager}
```

In the above config snippet ${PORT:9900} is declaring an environment variable called PORT. If this is not present in the environment at runtime, the default value of 9900 will be used.


## Environment vars

The following environment variables are used to configure this service:

| Env var   | example   |  description   |
| ------------- | ------------- | ------------- |
| PORT | 9900 | port number of service |
| CONTEXT_PATH | /ms-contactmanager | url context path
| SSL_ENABLED | true | enable https or not |
| KEYSTORE_PASSWORD | pa55w0rd | password for generating self signed certificate |
| JSON_LOG_FORMAT_ENABLED | true | flag to enable json log output |
| ACTUATOR_ENDPOINTS | health,info,metrics,prometheus | actuator endpoints to expose |
| DETAILED_MESSAGE_ENABLED | true | turn on or off detailed error message |
| MONGO_HOST | localhost:28000 | database host name |
| MONGO_DB_NAME | dbname | database name |  
| MONGO_TIMEOUT | 4000 | connection timeout |  
| MONGO_USER | service user | database user |  
| MONGODB_USER_PASSWORD | ilovemonkeys | database password |  


## Test

To run tests on the service, use the commands detailed in the table below:

| command   | description   | 
| ------------- | ------------- | 
| mvn test      | run unit tests only | 
| mvn verify -DskipUnit -Ddependency-check.skip | run integration tests only | 
| mvn verify | run all tests (unit, integration, dependency check) | 
| mvn verify -DskipTests | run dependency check without running integration and unit tests |


If you want to skip both unit and integration tests from a specific maven phase then simply add the __-DskipTests__ flag to your command. e.g. _mvn package -DskipTests_


### Unit testing

The surefire plugin and spring-boot-starter-test (JUnit 5, AssertJ, Hamcrest, Mockito) are included for unit testing. The Jacoco plugin is also included to generate test coverage reports. Jacoco will place its generated reports in the target/site/jacoco directory.

If you want to skip the unit test execution from any maven phase then add the __-DskipUnit__ option to the command, e.g. _mvn package -DskipUnit_


### Integration & API testing

The Failsafe plugin is included to run integration tests. Failsafe will recognize any classes with the following naming patterns as an integration tests:

* \*\*/IT*.java – all Java filenames that start with "IT".
* \*\*/*IT.java – all Java filenames that end with "IT".
* \*\*/*ITCase.java – all Java filenames that end with "ITCase".

The RestAssured dependency is included for API testing. For further information on its usage see: http://rest-assured.io/

If you want to skip the unit test execution from any maven phase then add the __-DskipUnit__ option to the command, e.g. _mvn package -DskipITs_

The fabric8 docker-maven-plugin has been been included for use with integration testing. This plugin allows you to start and stop docker containers and bind this process to maven phases. In this project the start and build goals are bound to the pre-integration-test phase, this allows us to start docker containers before the integration tests are run. The stop goal is bound to the post-integration-test allowing us to stop all containers once the integration tests have finished. The current project will start the database in a docker container before running integration tests, then stop it again after completion. For more information on the docker-maven-plugin see https://dmp.fabric8.io/.


### Security testing

The OWASP dependency-check-maven plugin is included for dependency vulnerability scanning. It will be executed during the verify stage and its report placed in the target directory.

To skip dependency checking add the __-Ddependency-check.skip__ flag to your maven command e.g. _mvn verify _-Ddependency-check.skip_


## Logging

Logging is managed with Logback. By default this will log out in a standard java log format.

Logback will write both application and access logs to the console. File appenders are not used, since Docker is the intended deployment mechanism.

A JSON log pattern is also included in the Logback config. This can be enabled by setting an environment variable: JSON_LOG_FORMAT_ENABLED=true


## Metrics

The actuator metrics endpoint has been enabled to show metric information for the service. In addition to this, the prometheus endpoint has been enabled to allow a prometheus server to scrape data from the service.


## Actuator

The following actuator endpoints are exposed by default within the project: health, info, metrics

* /actuator/health = This will return status 200 if the service is running and healthy. It will return 503 if any of the health checks fail.
* /actuator/info = This will return basic information about the service taken from the pom such as application name, description, version and java version.
* /actuator/metrics = This endpoint will return a list of available metrics. To view specific metrics, just append the metric name to the end of the url, e.g. actuator/metrics/system.cpu.usage

Exposing of actuator endpoints can be controlled by setting the environment variable ACTUATOR_ENDPOINTS=health,info,metrics,prometheus

For more information on Actuator, see: https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html


## Dependency management

The versions-maven-plugin is used to detect available version updates for maven dependencies. The version check can be run using the maven command:

`mvn versions:display-dependency-updates`

If you would like to update to the latest release version for all dependencies, then run the maven command:

`mvn versions:use-latest-releases`

To only update to the next release versions, use the maven command:

`mvn versions:use-next-releases`


## Maintenance and Contributing

This project is maintained by TEAM NAME (contact details above)

See [CONTRIBUTING.md](./CONTRIBUTING.md) for contributing to this project.


## Licence

This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details  
