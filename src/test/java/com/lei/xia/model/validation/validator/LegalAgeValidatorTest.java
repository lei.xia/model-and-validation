package com.lei.xia.model.validation.validator;

import com.lei.xia.model.validation.annotation.LegalAge;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class LegalAgeValidatorTest {

  private LegalAgeValidator cut;

  @BeforeEach
  void beforeEach() {
    cut = new LegalAgeValidator();
  }

  @ParameterizedTest(name = "{index}: min age = {0} max aage = {1} actual age = {2} isValid = {3}")
  @MethodSource(value = "testCases")
  void test_illegal_age_with_in_out_side_dates(int min, int max, int actual, boolean expected) {
    var legalDate = mock(LegalAge.class);
    when(legalDate.allowFuture()).thenReturn(false);
    when(legalDate.min()).thenReturn(min);
    when(legalDate.max()).thenReturn(max);

    cut.initialize(legalDate);

    var ctx = mock(ConstraintValidatorContext.class);
    assertEquals(expected, cut.isValid(LocalDate.now().minusYears(actual), ctx));
  }

  static Stream<Arguments> testCases() {
    return Stream.of(
        Arguments.of(16, 16, 16, true),
        Arguments.of(16, 20, 17, true),
        Arguments.of(16, 20, 18, true),
        Arguments.of(16, 20, 19, true),
        Arguments.of(16, 19, 20, false));
  }

  @Test
  void test_illegal_date_is_in_future() {
    var legalDate = mock(LegalAge.class);
    when(legalDate.allowFuture()).thenReturn(false);

    cut.initialize(legalDate);

    var ctx = mock(ConstraintValidatorContext.class);
    var actual = cut.isValid(LocalDate.now().plusDays(1), ctx);
    assertFalse(actual);
  }

  @Test
  void test_min_is_zero() {
    var legalDate = mock(LegalAge.class);
    when(legalDate.allowFuture()).thenReturn(false);
    when(legalDate.min()).thenReturn(0);
    when(legalDate.max()).thenReturn(0);

    cut.initialize(legalDate);

    var ctx = mock(ConstraintValidatorContext.class);
    var actual = cut.isValid(LocalDate.now(), ctx);

    assertTrue(actual);
  }

  @Test
  void should_throw_illegal_arg_exception() {
    var legalDate = mock(LegalAge.class);
    when(legalDate.allowFuture()).thenReturn(false);
    when(legalDate.min()).thenReturn(10);
    when(legalDate.max()).thenReturn(0);

    var actual = assertThrowsExactly(IllegalArgumentException.class, () -> cut.initialize(legalDate));

    assertEquals("Legal age min [10] allowed is greater than max [0] allowed", actual.getMessage());
  }
}
