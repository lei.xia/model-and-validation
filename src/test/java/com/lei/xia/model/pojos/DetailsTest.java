package com.lei.xia.model.pojos;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class DetailsTest {

  @Test
  void should_fail_validation_with_illegal_age() {
    var subject = new Details();
    subject.setDob(LocalDate.now().minusYears(3));
    var actual = subject.validate();
    assertThat(actual).isFalse();
    var errorMessages = subject.getErrorMessages();
    errorMessages.forEach(System.out::println);
  }

  @Test
  void should_recursively_correct_errors() {
    var subject = new Details();
    subject.setDob(LocalDate.now().minusYears(3));
    var address = new Address();
    address.setHouseNumber("1");
    address.setPostcode("AB1 1CD");
    subject.setAddress(address);
    var actual = subject.validate();
    assertThat(actual).isFalse();
    var errorMessages = subject.getErrorMessages();
    errorMessages.forEach(System.out::println);
    assertThat(errorMessages.size()).isEqualTo(7);
  }

}
