package com.lei.xia.model;

import javax.validation.ConstraintViolation;
import java.util.Set;

public interface Data {

  Set<String> getErrorMessages();

  Set<ConstraintViolation<?>> getErrors();

  boolean validate();
}
