package com.lei.xia.model.pojos;

import com.lei.xia.model.validation.Validatable;
import com.lei.xia.model.validation.annotation.LegalAge;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
@Validated
public class Details extends Validatable {

  @NotNull(message = "First name is mandatory")
  @NotBlank(message = "First name can not be blank")
  @Size(max = 100, message = "First name can not exceed 100 characters")
  private String firstName;

  @NotNull(message = "Last name is mandatory")
  @NotBlank(message = "Last name can not be blank")
  @Size(max = 100, message = "Last name can not exceed 100 characters")
  private String lastName;

  @NotNull(message = "address must not null")
  @Valid
  private Address address;

  @NotNull(message = "DoB is mandatory")
  @LegalAge(
      min = 16,
      max = 100,
      message = "Dob does not meet pre set age requirement")
  private LocalDate dob;
}
