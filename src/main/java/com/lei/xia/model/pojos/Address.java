package com.lei.xia.model.pojos;

import com.lei.xia.model.validation.Validatable;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Validated
@Data
public class Address extends Validatable {

  @NotNull(message = "House number must not null")
  @NotBlank(message = "House number must not blank")
  private String houseNumber;

  @NotNull(message = "Road or street must not null")
  @NotBlank(message = "Road or street must not blank")
  private String line2;

  private String postcode;

}
