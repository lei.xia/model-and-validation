package com.lei.xia.model.validation.validator;

import com.lei.xia.model.validation.annotation.LegalAge;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.Period;

public class LegalAgeValidator implements ConstraintValidator<LegalAge, LocalDate> {
  private int min;
  private int max;
  private boolean allowFuture;

  @Override
  public void initialize(LegalAge legalAge) {
    this.min = legalAge.min();
    this.max = legalAge.max();
    if (this.max < this.min) {
      throw new IllegalArgumentException(String.
          format("Legal age min [%d] allowed is greater than max [%d] allowed", this.min, this.max));
    }
    this.allowFuture = legalAge.allowFuture();
  }

  @Override
  public boolean isValid(LocalDate dob, ConstraintValidatorContext ctx) {
    var now = LocalDate.now();
    if (dob.isAfter(now)) {
      return false;
    }
    var age = Period.between(dob, now).getYears();
    return age >= this.min && age <= this.max;
  }
}
