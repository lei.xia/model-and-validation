package com.lei.xia.model.validation;

import com.lei.xia.model.Data;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public abstract class Validatable implements Data {

  private Set<ConstraintViolation<Validatable>> errors = new HashSet<>();
  private final Set<String> errorMessages = new HashSet<>();
  private static final Validator validator;

  static {
    try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
      validator = factory.getValidator();
    }
  }

  public boolean validate() {
    errors = validator.validate(this);
    if (!errors.isEmpty()) {
      errors.forEach( e -> errorMessages.add(e.getMessage()));
    }
    return errorMessages.isEmpty();
  }

  public String errorsToString() {
    var builder = new StringBuilder();
    Optional.of(errorMessages).ifPresent(m -> builder.append(String.format(" %s ", m)));
    return builder.toString().trim();
  }

  public void collectErrorMessage(Data data) {
    errorMessages.addAll(Optional.ofNullable(data)
        .map(Data::getErrorMessages)
        .orElse(Collections.emptySet()));
  }

  public Set<ConstraintViolation<?>> getErrors() {
    return new HashSet<>(this.errors);
  }

  public Set<String> getErrorMessages() {
    return new HashSet<>(this.errorMessages);
  }

  protected void addErrorMessage(String errorMessage) {
    errorMessages.add(errorMessage);
  }
}
